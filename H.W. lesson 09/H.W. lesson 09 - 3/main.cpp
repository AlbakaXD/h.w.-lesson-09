#include <iostream>
#include "BSNode3.h"

int main()
{
    //Part A
    //BSNode<int>* bs = new BSNode<int>(6);
    //bs->insert(2);
    //bs->insert(8);
    //bs->insert(3);
    //bs->insert(5);
    //bs->insert(9);
    //bs->insert(6);

    //Part B
    int arrINT[15] = { 15, 14, 13, 12, 11, 10, 9,8,7,6,5,4,3,2,1 };
    for (int i = 0; i < 15; i++)
    {
        std::cout << arrINT[i] << " ";
    }
    std::cout << std::endl;

    BSNode<int>* binaryTree = new BSNode <int>(arrINT[0]);
    for (int i = 1; i < 15; i++)
    {
        binaryTree->insert(arrINT[i]);
    }


    std::string arraySTR[15] = { "z", "y", "x", "t", "a", "s", "r", "u", "b" , "c", "g", "l", "J", "f", "m"};
    for (int i = 0; i < 15; i++)
    {
        std::cout << arraySTR[i] << " ";
    }
    std::cout << std::endl;

    BSNode<std::string>* binaryTree2 = new BSNode <std::string>(arraySTR[0]);
    for (int i = 1; i < 15; i++)
    {
        binaryTree2->insert(arraySTR[i]);
    }

    //Part C
    std::cout << "Sorted arrays: \n" << std::endl;
    binaryTree->printNodes();
    std::cout << "\n" << std::endl;
    binaryTree2->printNodes();
}