#pragma once
#ifndef BSNode3_H
#define BSNode3_H

#include <string>
#include <math.h>
#include <iostream>

template <class T>

class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;

	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const;

private:

	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //to count replicates
	int getCurrNodeDistFromInputNode(BSNode* node) const; //auxiliary function for getDepth

};

template <class T>
BSNode<T>::BSNode(T data)
{
	/*
	function builds a new node with given data
	input: data - string to insert to _data in node
	output: the initiated node
	*/
	this->_count = 0;
	this->_data = data;
	this->_right = nullptr;
	this->_left = nullptr;
}

template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	/*
	function builds a node according to an existing node
	input: other - an existing node
	output: the initiated node
	*/
	*this = other;
}

template <class T>
BSNode<T>::~BSNode()
{
	delete this->_left;
	delete this->_right;
	this->_data = "";
}

template <class T>
void BSNode<T>::insert(T value)
{
	/*
	function inserts a node into the binary tree
	input: value - the value of the new node
	output: none
	*/
	if (this->_data > value)
	{
		if (this->_left == nullptr)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			this->_left->insert(value);
		}
	}
	else if (this->_data < value)
	{
		if (this->_right == nullptr)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			this->_right->insert(value);
		}
	}
	else
	{
		this->_count = this->_count + 1;
	}
}

template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	this->_count = other._count;
	this->_data = other._data;
	this->_right = other._right;
	this->_left = other._left;
	return *this;
}

template <class T>
bool BSNode<T>::isLeaf() const
{
	/*
	function check rather the current node is a leaf
	input: none
	output: bool value rather the node is a leaf
	*/
	if (this->_left == nullptr && this->_right == nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	/*
	function returns current nodes _left
	*/
	return this->_left;
}

template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	/*
	function returns current nodes _right
	*/
	return this->_right;
}

template <class T>
bool BSNode<T>::search(T val) const
{
	if (this->_data == val)
	{
		return true;
	}
	else if (val > this->_data && this->_right != nullptr)
	{
		this->_right->search(val);
	}
	else if (val < this->_data && this->_left != nullptr)
	{
		this->_left->search(val);
	}
	return false;
}

template <class T>
int BSNode<T>::getHeight() const
{
	if (this == nullptr)
	{
		return -1;
	}
	else
	{
		int leftHeight = this->_left->getHeight();
		int rightHeight = this->_right->getHeight();

		if (leftHeight > rightHeight)
		{
			return (leftHeight + 1);
		}
		else
		{
			return (rightHeight + 1);
		}
	}
}

template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	if (&root == nullptr)
	{
		return -1;
	}
	int dist = -1;
	if ((root._data == this->_data) || (dist = this->getDepth(*root._left)) >= 0 || (dist = this->getDepth(*root._right)) >= 0)
	{
		return dist + 1;
	}

	return dist;
}

template <class T>
void BSNode<T>::printNodes() const
{
	if (this != NULL)
	{
		if (this->_left != NULL)
		{
			this->_left->printNodes();
		}
		std::cout << this->_data << " " << this->_count << std::endl;
		if (this->_right != NULL)
		{
			this->_right->printNodes();
		}
	}
}

#endif