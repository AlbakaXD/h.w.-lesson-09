#pragma once

#include <iostream>

template<class T>
int compare(T num1, T num2)
{
	if (num1 > num2)
	{
		return -1;
	}
	else if (num1 == num2)
	{
		return 0;
	}
	return 1;
}

	template<class T>
	void bubbleSort(T arr[], int len)
	{
		int i = 0, j = 0;
		for (int i = 0; i < len - 1; i++)
		{
			for (int j = 0; j < len - i - 1; j++)
			{
				if (arr[j] > arr[j + 1])
				{
					T temp = arr[j + 1];
					arr[j + 1] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

template<typename T>
void PrintArray(T arr[], int n)
{
	for (int i = 0; i < n; ++i)
	{
		std::cout << arr[i] << std::endl;
	}
}